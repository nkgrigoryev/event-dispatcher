export default class EventDispatcher {
    private readonly _dispatcher: EventTarget;

    constructor() {
        this._dispatcher = document.createTextNode(null);
    }

    public get addEventListener() {
        return this._dispatcher.addEventListener.bind(this._dispatcher);
    }

    public get removeEventListener() {
        return this._dispatcher.removeEventListener.bind(this._dispatcher);
    }

    public get dispatchEvent() {
        return this._dispatcher.dispatchEvent.bind(this._dispatcher);
    }
}