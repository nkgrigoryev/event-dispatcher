import '@casino/demo-css'
import './css/styles.css'
import EventDispatcher from "../";

class ColorChangeEvent extends CustomEvent {
    /**
     * @returns {string}
     */
    static get name() {
        return 'COLOR_CHANGE_EVENT';
    }

    constructor() {
        super(ColorChangeEvent.name);
    }
}

class Model extends EventDispatcher {
    /**
     * @returns {int}
     */
    static get DEFAULT_COLOR() {
        return 0x777777;
    }

    /**
     * @returns {int}
     */
    static get INTERVAL() {
        return 500;
    }

    constructor() {
        super();
        this._color = Model.DEFAULT_COLOR;
        setInterval(this._changeColor.bind(this),  Model.INTERVAL);
    }

    _changeColor() {
        this._color = Math.floor(Math.random() * 0xFFFFFF);
        this.dispatchEvent(new ColorChangeEvent());
    }

    get color() {
        return this._color;
    }
}


class View {
    /**
     * @returns {string}
     */
    static get CLASS() {
        return 'circle'
    }

    /**
     * @param container {HTMLElement}
     * @param model {Model}
     */
    constructor(container, model) {
        this._model = model;

        this._div = document.createElement('div');
        this._div.classList.add(View.CLASS);
        container.appendChild(this._div);
        this._model.addEventListener(ColorChangeEvent.name, this._onModelChangeColor.bind(this));
        this._onModelChangeColor();
    }

    _onModelChangeColor() {
        this._div.style.backgroundColor = `#${(this._model.color & 0x00FFFFFF).toString(16)}`
    }
}

let model = new Model();
let container = document.getElementById('circles');
for (let i = 0; i < 10; i++) {
    new View(container, model);
}