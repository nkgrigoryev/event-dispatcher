# README

## Installation
```bash
cd /path/to/project
yarn install
```

## Build demo
```bash
yarn webpack
```

## Use
```js
import EventDispatcher from '@casino/event-dispatcher';

class Model extends EventDispatcher {
    constructor() {
        super();
    }
    
    event() {
        this.dispatchEvent('event');
    }
}

let model = new Model();
model.addEventListener('event', () => console.log('event'));
model.event();
```